//
//  Defaults.swift
//  FS
//
//  Created by Black-Opium on 12.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import Foundation
import CoreLocation

class Defaults {
    static var location:CLLocation? = nil
    static var active:Bool = false
    
    static let First_start:String           = "First_start";
    static let Default_location:String      = "Default_location";
    static let Times_of_prayers:String      = "Times_of_prayers";
    static let Asr_juristic_method:String   = "Asr_juristic_Methods";
    static let Its_friday:String            = "Its_friday";
    static let Info_City_Name:String        = "Info_City_Name";
    static let Info_Mosque_Name:String      = "Info_Mosque_Name";
    static let Info_Updating_Date:String    = "Info_Updating_Date";
    
    static let Fajr_delta_azan:String           = "Fajr_delta_azan";
    static let Fajr_delta_iqama:String          = "Fajr_delta_iqama";
    static let Shuruq_delta_azan:String         = "Shuruq_delta_azan";
    static let Fajr_notification_azan:String    = "Fajr_notification_azan";
    static let Fajr_notification_iqama:String   = "Fajr_notification_iqama";
    static let Fajr_notification_shuruq:String  = "Fajr_notification_shuruq";
    static let Fajr_button_azan:String          = "Fajr_button_azan";
    static let Fajr_button_iqama:String         = "Fajr_button_iqama";
    static let Fajr_button_shuruq:String        = "Fajr_button_shuruq";
    
    static let standart:UserDefaults = UserDefaults(suiteName: "group.bo.fs")!
    
}
