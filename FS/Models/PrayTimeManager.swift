//
//  PrayTimeManager.swift
//  FS
//
//  Created by Black-Opium on 12.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import Foundation
import CoreLocation
import Adhan

class PrayTimeManager {
    
    static let currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 42.9779547, longitude: 47.4986374)
    static let date = Date()
    
    class func start() {
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let date = cal.dateComponents([.year, .month, .day], from: Date())
        let coordinates = Coordinates(latitude: 42.9779547, longitude: 47.4986374)
        var params = CalculationMethod.ummAlQura.params
        params.madhab = .hanafi
        if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            formatter.timeZone = .current
            NSLog("fajr %@", formatter.string(from: prayers.fajr))
            NSLog("sunrise %@", formatter.string(from: prayers.sunrise))
            NSLog("dhuhr %@", formatter.string(from: prayers.dhuhr))
            NSLog("asr %@", formatter.string(from: prayers.asr))
            NSLog("maghrib %@", formatter.string(from: prayers.maghrib))
            NSLog("isha %@", formatter.string(from: prayers.isha))
        }
    }
}
