//
//  PrayerCell.swift
//  FS
//
//  Created by Black-Opium on 11.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import UIKit

class PrayerCell: UITableViewCell {
    @IBOutlet weak var scrollView: PrayerScroll!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isUserInteractionEnabled = true
        scrollView.delaysContentTouches = false
    }
}
