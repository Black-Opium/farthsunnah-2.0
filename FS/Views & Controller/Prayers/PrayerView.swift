//
//  PrayerView.swift
//  FS
//
//  Created by Black-Opium on 11.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import UIKit

class PrayerView: UIView {
    @IBOutlet weak var alarmButton: UIButton!

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    
    var action = {}
    
    @IBAction func actionOn(_ sender: Any) {
        action()
    }
}
