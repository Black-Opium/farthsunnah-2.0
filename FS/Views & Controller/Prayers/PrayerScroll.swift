//
//  PrayerScroll.swift
//  FS
//
//  Created by Black-Opium on 12.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import UIKit

class PrayerScroll: UIScrollView {
    
    fileprivate var viewsCount: Int = 0
    
    func cyclicAddition(_ view: UIView, With width: CGFloat) {
        let x = width *  CGFloat(viewsCount)
        view.frame.origin = CGPoint(x: x, y: 0)
        view.frame.size.width = width
        addSubview(view)
        viewsCount += 1
    }
    
    func setContentSizeAt(_ width: CGFloat) {
        let contentWidth: CGFloat = width * CGFloat(viewsCount)
        contentSize = CGSize(width: contentWidth, height: 0)
    }
}
