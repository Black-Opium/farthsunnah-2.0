//
//  FajrCellContentView.swift
//  FS
//
//  Created by Black-Opium on 03.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//
import UIKit

class FajrViews: Prayer {
    
    var sunrise: PrayerView!
    
    override init() {
        super.init()
        azan = (UINib(nibName: "PrayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PrayerView)
        azan.title.text = NSLocalizedString("Azan", comment: "Fajr time category")
        azan.timeLable.text = "8:19"
        azan.action = {
            print("Hello, world")
            self.azan.title.text = "Fajr Azan"
        }
        
        iqama = (UINib(nibName: "PrayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PrayerView)
        iqama.title.text = NSLocalizedString("Iqama", comment: "Fajr time category")
        iqama.timeLable.text = "8:55"
        
        sunrise = (UINib(nibName: "PrayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PrayerView)
        sunrise.title.text = NSLocalizedString("Sunrise", comment: "Fajr time category")
        sunrise.timeLable.text = "8:19"
        sunrise.action = {
            print("Hello, world")
            self.sunrise.title.text = "Fajr Sunrise"
        }
    }
}
