//
//  PrayerViews.swift
//  FS
//
//  Created by Black-Opium on 11.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import UIKit

class Prayers {
    
    var count: Int! = 0
    
    var views: [Int:[PrayerView]]? = nil
    
    init() {
        views = [
            0:[
                FajrViews().azan,
                FajrViews().iqama,
                FajrViews().sunrise],
        ]
        count = views?.count
    }
    
}
