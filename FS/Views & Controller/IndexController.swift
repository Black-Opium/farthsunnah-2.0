//
//  ViewController.swift
//  FS
//
//  Created by Black-Opium on 02.09.2018.
//  Copyright © 2018 BO. All rights reserved.
//

import UIKit

class IndexController: UIViewController {

    @IBOutlet var globalTableView: UITableView!
    
    let prayers: Prayers = Prayers()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        globalTableView.tableFooterView = UIView()
        globalTableView.allowsSelection = false
        globalTableView.delaysContentTouches = false
        PrayTimeManager.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension IndexController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prayers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "prayerCell", for: indexPath) as! PrayerCell
        let width = cell.frame.width
        let views = prayers.views![indexPath.row]
        for prayerView in views! {
            cell.scrollView.cyclicAddition(prayerView, With: width)
        }
        cell.scrollView.setContentSizeAt(width)
        return cell
    }
    
}
